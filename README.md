Members: Khalid Douch & Güney Karaagac

What is in our Application?
- All minimum requirements
    - Three Classes Country, States, GovernedRegion
    - with Create, Read, Update, Delete

- Exception Handling & Correct DataInput:
    - Program prevents entering empty Strings or wrong datatypes
    - Objects cant be created if wrong Data & nothing is entered
    - Program marks text for wrong input or Correct input

- Good user interface appereance:
    - a structured, clear and good visualed Application with Feedbacks to make it easier for the User
    - with css

- Save Data to disk:
    - saves entered Date to File
    - loads entered Date from File (if exists)
