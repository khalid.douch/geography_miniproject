package geography_Model;


public class State extends GovernedRegion {
	private static int numberOfStates = 0;
	private int stateID = 0;
	private String name;
	private Country country;

	public State(String name, Country country, double area, String formOfGovernment, int population) {
		stateID = numberOfStates;
		this.name = name;
		this.country = country;
		this.area = area;
		this.formOfGovernment = GovernmentEnum.valueOf(formOfGovernment);
		this.population = population;
		numberOfStates++;

	}

	public String toString() {
		return name;
	}

	public String toTxt() {
		return this.getStateID() + ";" + this.getName() + ";" + this.getCountry().getCountryID() + ";" + this.getArea()
				+ ";" + this.getFormOfGovernment() + ";" + this.getPopulation();
	}

	public String getName() {
		return name;
	}

	public Country getCountry() {
		return country;
	}

	public int getStateID() {
		return stateID;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public void setStateID(int stateID) {
		this.stateID = stateID;
	}

}
