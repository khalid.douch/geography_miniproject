package geography_Model;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.swing.JFrame;
import javax.swing.JLabel;
import geography_Model.GovernedRegion.GovernmentEnum;
import geography_View.CountryTab;
import geography_View.CountryList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class GeographyModel {

	private ObservableList<State> stateList;
	private ObservableList<Country> countryList;
	private CountryTab countryTab;
	private CountryList country;

	private static String FILE_COUNTRY = "C:/temp/Country.txt";
	private static String FILE_STATE = "C:/temp/State.txt";
	private String saveFileData;

	public GeographyModel() {
		stateList = FXCollections.observableArrayList();
		countryList = FXCollections.observableArrayList();
	}

	public ArrayList<Country> readFile() {
		try {
			Scanner fileReader = new Scanner(new File(FILE_COUNTRY));
			fileReader.useDelimiter(";");
			ArrayList<Country> c = new ArrayList();
			while (fileReader.hasNext()) {
				String line = fileReader.nextLine();

				c.add(this.readLine(line));
			}
			return c;
		} catch (FileNotFoundException e) {
			String data = "Save file does not exist";
		} catch (IOException e) {
			String data = e.getClass().toString();
		}
		return null;
	}

	public ArrayList<State> readFileState() {
		try {
			Scanner fileReader = new Scanner(new File(FILE_STATE));
			fileReader.useDelimiter(";");
			ArrayList<State> s = new ArrayList<>();
			while (fileReader.hasNext()) {
				String line = fileReader.nextLine();

				s.add(this.readLineState(line));
			}
			return s;

		} catch (FileNotFoundException e) {
			String data = "Save file does not exist";
		} catch (IOException e) {
			String data = e.getClass().toString();
		}
		return null;
	}

	public Country readLine(String line) {
		Scanner lineScanner = new Scanner(line);
		lineScanner.useDelimiter(";");
		int id = 0;
		String name = "";

		id = Integer.parseInt(lineScanner.next());
		name = lineScanner.next();
		Country cc = new Country(id, name);
		countryList.add(cc);

		return cc;
	}

	public void createGUIload() {
		JFrame frame = new JFrame("Load Countries and States");// Open Info Windows
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.setPreferredSize(new Dimension(800, 450));
		JLabel l = new JLabel(
				"<html>Data loaded from C:\\temp\\Country.txt<br>Data loaded from C:\\temp\\State.txt</html>");
		l.setPreferredSize(new Dimension(500, 100));
		Font labelFont = l.getFont();
		l.setFont(new Font(labelFont.getName(), Font.PLAIN, 40));
		frame.getContentPane().add(l, BorderLayout.CENTER);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public void writeFileCountry() {
		BufferedWriter saveFile = null;
		try {
			File file = new File(FILE_COUNTRY);

			if (!file.exists())
				file.createNewFile();

			FileWriter fileOut = new FileWriter(file);
			saveFile = new BufferedWriter(fileOut);
			for (Country c : countryList) {
				saveFile.write(c.toTxt() + "\n");
			}

			JFrame frame = new JFrame("Save Country");
			frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			frame.setPreferredSize(new Dimension(800, 450));

			JLabel l = new JLabel(
					"<html>Data saved to C:\\temp\\Country.txt<br>Data saved to C:\\temp\\State.txt</html>");
			l.setPreferredSize(new Dimension(500, 100));
			Font labelFont = l.getFont();
			l.setFont(new Font(labelFont.getName(), Font.PLAIN, 40));
			frame.getContentPane().add(l, BorderLayout.CENTER);
			frame.pack();
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);

		} catch (IOException exception1) {
			exception1.printStackTrace();
		} finally {
			try {
				if (saveFile != null)
					saveFile.close();
			} catch (Exception exception2) {
				System.out.println("File could not be created, Error: " + exception2);
			}
		}
	}

	public void writeFileState() {
		BufferedWriter saveFile = null;
		try {
			File file = new File(FILE_STATE);

			if (!file.exists())
				file.createNewFile();

			FileWriter fileOut = new FileWriter(file);
			saveFile = new BufferedWriter(fileOut);
			for (State s : stateList) {
				saveFile.write(s.toTxt() + "\n");
			}
		} catch (IOException exception1) {
			exception1.printStackTrace();
		} finally {
			try {
				if (saveFile != null)
					saveFile.close();
			} catch (Exception exception2) {
				System.out.println("File could not be created, Error: " + exception2);
			}
		}
	}

	public State readLineState(String line) {

		Scanner lineScanner = new Scanner(line);
		lineScanner.useDelimiter(";");

		int stateID;
		String name;
		int countryID;
		Double area;
		String government;
		int population;

		stateID = Integer.parseInt(lineScanner.next());
		name = lineScanner.next();
		countryID = Integer.parseInt(lineScanner.next());
		area = Double.parseDouble(lineScanner.next());
		government = lineScanner.next();
		population = Integer.parseInt(lineScanner.next());

		State newState = new State(name, getCountryByCountryID(countryID), area, government, population);
		stateList.add(newState);

		return newState;
	}

	public void setSaveFile(String in) {
		saveFileData = "Save file contents: " + in;
	}

	public String getSaveFile() {
		return saveFileData;
	}

	public void createCountry(String name) {
		countryList.add(new Country(name));
	}

	public void saveCountry(int countryID, String name) {
		int index = getCountryListIndexOfID(countryID);
		countryList.get(index).setName(name);
	}

	public int deleteCountry(int countryID) {
		int index = getCountryListIndexOfID(countryID);
		countryList.remove(index);
		return index;
	}

	public void createState(String name, Country country, double area, String formOfGovernment, int population) {
		stateList.add(new State(name, country, area, formOfGovernment, population));
	}

	public void saveState(int stateID, String name, Country country, double area, String formOfGovernment,
			int population) {
		int index = getStateListIndexOfID(stateID);
		stateList.get(index).setName(name);
		stateList.get(index).setCountry(country);
		stateList.get(index).setFormOfGovernment(GovernmentEnum.valueOf(formOfGovernment));
		stateList.get(index).setArea(area);
		stateList.get(index).setPopulation(population);
	}

	public int deleteState(int stateID) {
		int index = getStateListIndexOfID(stateID);
		stateList.remove(index);
		return index;
	}

	public ArrayList<State> getStateList() {
		return (ArrayList<State>) stateList;
	}

	public int getStateListSize() {
		int size = 0;
		if (stateList != null) {
			size = stateList.size();
		}
		return size;
	}

	public ObservableList<State> getStateListObservable() {
		return stateList;
	}

	public ArrayList<Country> getCountryList() {
		return (ArrayList<Country>) countryList;
	}

	public ObservableList<Country> getCountryListObservable() {
		return countryList;
	}

	public ArrayList<String> getGovernmentList() {
		List<String> enumNames = Stream.of(GovernmentEnum.values()).map(Enum::name).collect(Collectors.toList());
		return (ArrayList<String>) enumNames;
	}

	public State getLastState() {
		return stateList.get(stateList.size() - 1);
	}

	public State getStateByStateID(int stateID) {
		int index = getStateListIndexOfID(stateID);

		return stateList.get(index);
	}

	public int getStateListIndexOfID(int stateID) {
		int temp = 0;
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateID() == stateID) {
				temp = i;
			}
		}
		return temp;
	}

	public int getCountryListIndexOfID(int countryID) {
		int temp = 0;
		for (int i = 0; i < countryList.size(); i++) {
			if (countryList.get(i).getCountryID() == countryID) {
				temp = i;
			}
		}
		return temp;
	}

	public Country getLastCountry() {
		return countryList.get(countryList.size() - 1);
	}

	public Country getCountryByCountryID(int countryID) {
		int index = getCountryListIndexOfID(countryID);
		return countryList.get(index);
	}

	public int getStateListIndexOfCountryID(int countryID) {
		int temp = 0;
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getCountry().getCountryID() == countryID) {
				temp = i;
			}
		}

		return temp;
	}

	public State getStateByCountryID(int countryID) {
		int index = getStateListIndexOfCountryID(countryID);
		return stateList.get(index);
	}

	public Country getCountryFromList(int countryID, ObservableList<Country> items) {
		Country country = null;
		for (int i = 0; i < items.size(); i++) {
			if (countryID == items.get(i).getCountryID()) {
				country = items.get(i);
			}
		}

		return country;
	}

	public ArrayList<Integer> deleteStateByCountry(Country country) {
		ArrayList<Integer> temp = new ArrayList<Integer>();
		if (country.getStateList().size() > 0) {
			for (State state : country.getStateList()) {
				temp.add(state.getStateID());
			}
		}
		return temp;
	}

}
