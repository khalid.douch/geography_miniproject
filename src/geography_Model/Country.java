package geography_Model;

import java.util.ArrayList;

import geography_Model.State;


public class Country {
	private static int numberOfCountries = 0;
	private int countryID = 0;
	private String name;
	private ArrayList<State> stateList;

	public Country(String name, ArrayList<State> stateList) {
		countryID = numberOfCountries;
		this.name = name;
		this.stateList = stateList;
		numberOfCountries++;
	}

	public Country(String name) {
		countryID = numberOfCountries;
		this.name = name;
		this.stateList = new ArrayList<State>();
		numberOfCountries++;
	}

	public Country(int countryID, String name) {
		numberOfCountries = countryID;
		this.countryID = countryID;
		this.name = name;
		this.stateList = new ArrayList<State>();
		numberOfCountries++;
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return name;
	}

	public String toTxt() {
		return this.countryID + ";" + this.name;
	}

	public void addSate(State state) {
		stateList.add(state);
	}

	public int getCountryID() {
		return countryID;
	}

	public void setCountryID(int countryID) {
		this.countryID = countryID;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setState(State state) {
		stateList.set(getStateIndexFromArray(state), state);
	}

	public int getStateIndexFromArray(State state) {
		int index = 0;
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateID() == state.getStateID()) {
				index = i;
			}
		}
		return index;
	}

	public void deleteState(int index) {
		stateList.remove(index);
	}

	public ArrayList<State> getStateList() {
		return stateList;
	}

}