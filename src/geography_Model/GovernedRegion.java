package geography_Model;

public abstract class GovernedRegion {
	protected double area;
	protected GovernmentEnum formOfGovernment;
	protected int population;

	public double getArea() {
		return area;
	}

	public GovernmentEnum getFormOfGovernment() {
		return formOfGovernment;
	}

	public int getPopulation() {
		return population;
	}

	public void setArea(double area) {
		this.area = area;
	}

	public void setPopulation(int population) {
		this.population = population;
	}

	public void setFormOfGovernment(GovernmentEnum formOfGovernment) {
		this.formOfGovernment = formOfGovernment;
	}

	public enum GovernmentEnum {
		Democracy, Monarchie, Communism, Dictatorship
	}
}
