package geography_View;

import java.util.ArrayList;

import geography_Model.Country;
import geography_Model.GeographyModel;
import geography_Model.State;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class CountryTab extends HBox {

	private ListView<Country> countryListView;
	private ArrayList<State> stateList;
	private VBox vbox1;
	private VBox vbox2;
	private CountryList countryList;
	private GeographyModel model;
	private int countryID;
	private TextField nameField;
	private ListView<State> stateListViewOfCountry;
	private Label stateListLabel;
	private Label countryListLabel;
	private ArrayList<Country> tempList;

	public CountryTab(GeographyModel model) {
		countryListView = new ListView<>();
		vbox1 = new VBox();
		tempList = new ArrayList<Country>();
		countryList = new CountryList(model);
		countryListLabel = new Label("Countries");
		countryListLabel.setId("stateListLabel");
		this.model = model;

		vbox2 = new VBox();
		nameField = new TextField();

		stateList = new ArrayList<State>();
		stateListViewOfCountry = new ListView<>();
		stateListLabel = new Label("States of the Country");
		stateListLabel.setId("stateListLabel");

		countryListView.setPrefWidth(100);
		stateListViewOfCountry.setPrefWidth(100);
		countryListView.setPrefHeight(295);
		stateListViewOfCountry.setPrefHeight(295);
		vbox1.setSpacing(5);
		Region spacer1 = new Region();
		spacer1.setPrefWidth(25);
		Region spacer2 = new Region();
		spacer2.setPrefWidth(10);
		vbox1.getChildren().addAll(countryListLabel, countryListView);
		vbox1.setSpacing(5);
		vbox2.getChildren().addAll(stateListLabel, stateListViewOfCountry);
		vbox2.setSpacing(5);
		this.getChildren().addAll(countryList, spacer1, vbox1, spacer2, vbox2);
	}

	public CountryList getCountryList() {
		return countryList;
	}

	public void setSelectedFields(Country country) {
		this.countryID = country.getCountryID();
		nameField.setText(country.getName());
		stateListViewOfCountry.getItems().setAll(country.getStateList());
		if (country.getStateList().size() > 0) {
			stateListViewOfCountry.setVisible(true);
			stateListLabel.setVisible(true);
		}
	}

	public int getCountryID() {
		return countryID;
	}

	public String getName() {
		return nameField.getText();
	}

	public void clearFields() {
		nameField.setText("");
	}

	public void addListItem(ObservableList<Country> country) {
		tempList.addAll(country);
		countryListView.getItems().addAll(country);
	}

	public ListView<Country> getCountryListView() {
		return countryListView;
	}

	public void addListItem(Country country) {
		tempList.add(country);
		countryListView.getItems().add(country);
	}

	public void updateListItem(Country country) {
		tempList.set(model.getCountryListIndexOfID(country.getCountryID()), country);
		countryListView.getItems().set(model.getCountryListIndexOfID(country.getCountryID()), country);
	}

	public void deleteListItem(int index) {
		countryListView.getItems().remove(index);
	}

}
