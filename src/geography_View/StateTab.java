package geography_View;

import java.util.ArrayList;

import geography_Model.GeographyModel;
import geography_Model.State;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class StateTab extends HBox {

	private ListView<State> stateListView;
	private VBox vBox;
	private StateList stateList;
	private GeographyModel model;
	private Label stateListLabel;

	protected ArrayList<State> tempList;

	public StateTab(GeographyModel model) {
		this.model = model;
		stateListView = new ListView<State>();
		vBox = new VBox();
		stateList = new StateList(model);
		tempList = new ArrayList<State>();
		stateListLabel = new Label("States");
		stateListLabel.setId("stateListLabel");
		stateListView.getItems().addAll(tempList);
		stateListView.setPrefWidth(100);
		stateListView.setPrefHeight(295);
		vBox.getChildren().addAll(stateListLabel, stateListView);
		vBox.setSpacing(5);
		Region spacer1 = new Region();
		spacer1.setPrefWidth(25);
		this.getChildren().addAll(stateList, spacer1, vBox);
	}

	public StateList getStateList() {
		return stateList;
	}

	public void addListItem(State state) {
		tempList.add(state);
		stateListView.getItems().add(state);
	}

	public void updateListItem(int stateIndex, State state) {
		tempList.set(stateIndex, state);
		stateListView.getItems().set(stateIndex, state);
	}

	public void addListItem(ObservableList<State> state) {
		tempList.addAll(state);
		stateListView.getItems().addAll(state);
	}

	public void deleteListItem(int index) {
		stateListView.getItems().remove(index);

	}

	public ListView<State> getStateListView() {
		return stateListView;
	}

}
