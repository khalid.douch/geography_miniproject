package geography_View;

import java.util.ArrayList;

import geography_Model.Country;
import geography_Model.GeographyModel;
import geography_Model.State;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class CountryList extends HBox {

	private GeographyModel model;
	private Label name;
	private TextField nameField;
	private Button update, delete, cancel;
	private Button create;

	private Button save, load;

	private HBox hbox1;
	private HBox hbox2;
	private HBox hbox3;
	private VBox vbox1;

	public CountryList(GeographyModel model) {
		name = new Label("Name");
		nameField = new TextField();
		update = new Button("Update");
		update.setId("button-edit");
		delete = new Button("Delete");
		delete.setId("button-edit");
		cancel = new Button("Cancel");
		cancel.setId("button-edit");
		create = new Button("Create");
		setVisibilityCreate();
		create.setDisable(true);

		save = new Button("Save");
		load = new Button("Load");

		hbox1 = new HBox();
		hbox2 = new HBox();
		hbox3 = new HBox();
		vbox1 = new VBox();

		Region spacer1 = new Region();
		spacer1.setPrefHeight(130);
		Region spacer2 = new Region();
		spacer2.setPrefWidth(65);
		Region spacer3 = new Region();
		spacer3.setPrefWidth(60);
		Region spacerTop = new Region();
		spacerTop.setPrefHeight(25);

		hbox1.getChildren().addAll(name, nameField);
		hbox1.setSpacing(5);
		hbox2.getChildren().addAll(cancel, delete, spacer2, update);
		hbox2.setSpacing(5);
		hbox3.getChildren().addAll(load, save, spacer3, create);
		hbox3.setSpacing(5);
		vbox1.getChildren().addAll(spacerTop, hbox1, hbox2, spacer1, hbox3);
		vbox1.setSpacing(5);

		this.getChildren().addAll(vbox1);
	}

	public Button getCancel() {
		return cancel;
	}

	public Button getCreateCountryButton() {
		return create;
	}

	public Button getSave() {
		return save;
	}

	public Button getLoad() {
		return load;
	}

	public Button getUpdateCountryButton() {
		return update;
	}

	public Button getDeleteCountryButton() {
		return delete;
	}

	public String getName() {
		return nameField.getText();
	}

	public TextField getNameTextField() {
		return nameField;
	}

	public void clearFields() {
		nameField.setText("");
	}

	public void setVisibilityCreate() {
		create.setVisible(true);
		update.setVisible(false);
		delete.setVisible(false);
		cancel.setVisible(false);
	}

	public void setSelectedFields(Country country) {
		nameField.setText(country.getName());
	}

	public void setVisibilityUpdate() {
		update.setVisible(true);
		delete.setVisible(true);
		cancel.setVisible(true);
	}
}
