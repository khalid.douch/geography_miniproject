package geography_View;

import geography_Model.GeographyModel;
import javafx.scene.layout.VBox;

public class GeographyView extends VBox {
	private GeographyModel model;
	private TabView tabView;

	public GeographyView(GeographyModel model) {
		this.model = model;

		tabView = new TabView(model);

		this.getChildren().addAll(this.tabView);
	}

	public TabView getTabView() {
		return tabView;
	}

}
