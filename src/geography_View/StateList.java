package geography_View;

import geography_Model.Country;
import geography_Model.GeographyModel;
import geography_Model.State;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class StateList extends VBox {

	private GeographyModel model;
	private int stateID;
	private HBox hbox1;
	private HBox hbox2;
	private HBox hbox3;
	private HBox hbox4;
	private HBox hbox5;
	private HBox hbox6;
	private HBox hbox7;

	private TextField nameField;
	private TextField areaField;
	private Label name;
	private Label area;
	private Label government;
	private Label population;
	private Label country;
	private Button update, delete, cancel;
	private Button save, create, load;
	private ChoiceBox<String> governmentField;
	private ChoiceBox<Country> countryBox;
	private TextField populationField;

	public StateList(GeographyModel model) {
		this.model = model;
		hbox1 = new HBox();
		hbox2 = new HBox();
		hbox3 = new HBox();
		hbox4 = new HBox();
		hbox5 = new HBox();
		hbox6 = new HBox();
		hbox7 = new HBox();

		name = new Label("Name");
		area = new Label("Area");
		government = new Label("Government");
		population = new Label("Population");
		nameField = new TextField();
		areaField = new TextField();
		update = new Button("Update");
		update.setId("button-edit");
		delete = new Button("Delete");
		delete.setId("button-edit");
		cancel = new Button("Cancel");
		cancel.setId("button-edit");
		create = new Button("Create");
		save = new Button("Save");
		load = new Button("Load");
		country = new Label("Country");
		create.setDisable(true);

		governmentField = new ChoiceBox<String>();
		governmentField.getItems().addAll(model.getGovernmentList());
		governmentField.getSelectionModel().select(0);
		populationField = new TextField();

		countryBox = new ChoiceBox<Country>(model.getCountryListObservable());

		Region spacer1 = new Region();
		spacer1.setPrefHeight(10);
		Region spacer2 = new Region();
		spacer2.setPrefWidth(65);
		Region spacer3 = new Region();
		spacer3.setPrefWidth(60);
		Region spacerTop = new Region();
		spacerTop.setPrefHeight(25);

		hbox1.getChildren().addAll(name, nameField);
		hbox1.setSpacing(5);
		hbox2.getChildren().addAll(country, countryBox);
		hbox2.setSpacing(5);
		hbox3.getChildren().addAll(area, areaField);
		hbox3.setSpacing(5);
		hbox4.getChildren().addAll(government, governmentField);
		hbox4.setSpacing(5);
		hbox5.getChildren().addAll(population, populationField);
		hbox5.setSpacing(5);
		hbox6.getChildren().addAll(cancel, delete, spacer2, update);
		hbox6.setSpacing(5);
		hbox7.getChildren().addAll(load, save, spacer3, create);
		hbox7.setSpacing(5);

		this.setSpacing(5);
		this.getChildren().addAll(spacerTop, hbox1, hbox2, hbox3, hbox4, hbox5, hbox6, spacer1, hbox7);
		setVisibilityCreate();

	}

	public Button getCancel() {
		return cancel;
	}

	public Button getCreate() {
		return create;
	}

	public Button getLoad() {
		return load;
	}

	public String getName() {
		return nameField.getText();
	}

	public double getArea() {
		return Double.parseDouble(areaField.getText());
	}

	public String getGovernment() {
		return governmentField.getSelectionModel().getSelectedItem();
	}

	public int getPopulation() {
		return Integer.parseInt(populationField.getText());
	}

	public void setVisibilityCreate() {
		create.setVisible(true);
		update.setVisible(false);
		delete.setVisible(false);
		cancel.setVisible(false);
	}

	public void setVisibilityUpdate() {
		update.setVisible(true);
		delete.setVisible(true);
		cancel.setVisible(true);
	}

	public void clearFields() {
		nameField.setText("");
		areaField.setText("");
		governmentField.getSelectionModel().select(0);
		populationField.setText("");

	}

	public Button getSave() {
		return save;
	}

	public Button getUpdate() {
		return update;
	}

	public Button getDelete() {
		return delete;
	}

	public int getStateID() {
		return stateID;
	}

	public void setStateID(int stateID) {
		this.stateID = stateID;
	}

	public void setSelectedFields(State state) {
		this.stateID = state.getStateID();
		nameField.setText(state.getName());
		areaField.setText(state.getArea() + "");
		countryBox.getSelectionModel().select(state.getCountry());
		governmentField.getSelectionModel().select(state.getFormOfGovernment().ordinal());
		populationField.setText(state.getPopulation() + "");
	}

	public ChoiceBox<Country> getCountryChoiceBox() {
		return countryBox;
	}

	public TextField getNameTextField() {
		return nameField;
	}

	public TextField getAreaTextField() {
		return areaField;
	}

	public TextField getPopulationTextField() {
		return populationField;
	}

	public void updateCountryChoiceBox(Country newCountry) {

		int i = countryBox.getItems().indexOf(newCountry);
		countryBox.getItems().remove(i);
		countryBox.getItems().add(i, newCountry);
	}

}
