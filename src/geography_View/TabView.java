package geography_View;

import geography_Model.GeographyModel;
import geography_View.CountryTab;
import geography_View.StateTab;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

public class TabView extends TabPane {

	private GeographyModel model;
	private CountryTab countryTabData;
	private StateTab stateTabData;
	private Tab countryTab, stateTab;

	public TabView(GeographyModel model) {
		this.model = model;
		countryTabData = new CountryTab(model);
		stateTabData = new StateTab(model);
		this.countryTab = new Tab("Country");
		this.stateTab = new Tab("States");
		this.countryTab.setContent(countryTabData);
		this.stateTab.setContent(stateTabData);
		this.countryTab.setClosable(false);
		this.stateTab.setClosable(false);

		this.getTabs().addAll(this.countryTab, this.stateTab);
	}

	public CountryTab getCountryTabList() {
		return countryTabData;
	}

	public StateTab getStateTabList() {
		return stateTabData;
	}

}
