package geography_miniproject;

import geography_Controller.GeographyController;
import geography_Model.GeographyModel;
import geography_View.GeographyView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class GeographyMVC extends Application {
	private GeographyView view;
	private GeographyModel model;
	private GeographyController controller;

	public static void main(String[] args) {
		launch(args);
	}

	public void start(Stage stage) throws Exception {
		model = new GeographyModel();
		view = new GeographyView(model);
		controller = new GeographyController(model, view);

		Scene scene = new Scene(view, 700, 395);

		scene.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
		stage.setScene(scene);
		stage.setTitle("Geography");
		stage.show();
	}
}
