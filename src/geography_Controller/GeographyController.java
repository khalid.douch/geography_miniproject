package geography_Controller;

import geography_Model.Country;
import geography_Model.GeographyModel;
import geography_Model.State;
import geography_View.CountryList;
import geography_View.CountryTab;
import geography_View.GeographyView;
import geography_View.StateList;
import geography_View.StateTab;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class GeographyController {
	private GeographyModel model;
	private GeographyView view;
	private CountryList countryList;
	private StateList stateList;
	private CountryTab countryTab;
	private StateTab stateTab;
	private boolean areaOK, nameOK, populationOK, stateCountryOK;

	public GeographyController(GeographyModel model, GeographyView view) {
		this.model = model;
		this.view = view;
		this.countryList = view.getTabView().getCountryTabList().getCountryList();
		this.stateList = view.getTabView().getStateTabList().getStateList();
		this.countryTab = view.getTabView().getCountryTabList();
		this.stateTab = view.getTabView().getStateTabList();
		this.areaOK = false;
		this.nameOK = false;
		this.populationOK = false;
		this.stateCountryOK = false;

		countryList.getCancel().setOnAction(e -> {
			countryList.setVisibilityCreate();
			countryList.clearFields();
		});
		
		stateList.getCancel().setOnAction(e -> {
			stateList.setVisibilityCreate();
			stateList.clearFields();
		});

		stateList.getLoad().setOnAction(e -> {
			model.readFile();
			countryTab.addListItem(model.getCountryListObservable());
			model.readFileState();
			stateTab.addListItem(model.getStateListObservable());
			for (int i = 0; i < model.getStateListSize(); i++) {
				model.getStateListObservable().get(i).getCountry().addSate(model.getStateListObservable().get(i));
			}
			model.createGUIload();
		});

		countryList.getLoad().setOnAction(e -> {
			model.readFile();
			countryTab.addListItem(model.getCountryListObservable());

			model.readFileState();
			stateTab.addListItem(model.getStateListObservable());
			for (int i = 0; i < model.getStateListSize(); i++) {
				model.getStateListObservable().get(i).getCountry().addSate(model.getStateListObservable().get(i));
			}
			model.createGUIload();
		});

		countryTab.getCountryListView().getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<Country>() {
					public void changed(ObservableValue<? extends Country> observable, Country oldValue,
							Country newValue) {
						countryList.setVisibilityUpdate();
						if (newValue != null) {
							countryTab.setSelectedFields(newValue);
							countryList.setSelectedFields(newValue);
						} else {
							countryList.clearFields();
							countryList.setVisibilityCreate();
						}
					}
				});

		countryList.getSave().setOnAction(e -> {
			model.writeFileCountry();
			model.writeFileState();
		});

		stateList.getSave().setOnAction(e -> {
			model.writeFileCountry();
			model.writeFileState();
		});

		countryList.getNameTextField().textProperty()
				.addListener((observable, oldValue, newValue) -> validateNameField(newValue));

		stateList.getNameTextField().textProperty()
				.addListener((observable, oldValue, newValue) -> validateNameFieldState(newValue));

		stateList.getAreaTextField().textProperty()
				.addListener((observable, oldValue, newValue) -> validateAreaFieldState(newValue));

		stateList.getPopulationTextField().textProperty()
				.addListener((observable, oldValue, newValue) -> validatePopulationFieldState(newValue));

		stateList.getCountryChoiceBox().getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<Country>() {
					public void changed(ObservableValue<? extends Country> observable, Country oldValue,
							Country newValue) {
						if (!stateList.getCountryChoiceBox().getSelectionModel().isEmpty()) {
							stateCountryOK = true;
						}
						if (nameOK && areaOK && populationOK && stateCountryOK) {
							stateList.getCreate().setDisable(false);
						}
					}
				});

		stateList.getCreate().setOnAction(e -> {
			createState();
		});

		stateList.getUpdate().setOnAction(e -> {
			updateState();
		});

		stateList.getDelete().setOnAction(e -> {
			deleteState();
		});


		countryList.getDeleteCountryButton().setOnAction(e -> {
			deleteCountry();
		});

		countryList.getCreateCountryButton().setOnAction(e -> {
			createCountry();
		});

		countryList.getUpdateCountryButton().setOnAction(e -> {
			saveCountry();
		});

		stateTab.getStateListView().getSelectionModel().selectedItemProperty().addListener(new ChangeListener<State>() {
			public void changed(ObservableValue<? extends State> observable, State oldValue, State newValue) {
				stateList.setVisibilityUpdate();
				if (newValue != null) {
					stateList.setSelectedFields(newValue);
				} else {
					stateList.clearFields();
					stateList.setVisibilityCreate();
				}

			}
		});

		countryTab.getCountryListView().getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<Country>() {
					public void changed(ObservableValue<? extends Country> observable, Country oldValue,
							Country newValue) {
						countryList.setVisibilityUpdate();
						if (newValue != null) {
							countryTab.setSelectedFields(newValue);
						} else {
							countryList.clearFields();
							countryList.setVisibilityCreate();
						}
					}

				});

	}

	public void validateNameField(String newValue) {
		if (newValue != null && !newValue.matches(".*\\d.*")) {
			countryList.getNameTextField().setId("inputOk");
			countryList.getCreateCountryButton().setDisable(false);
		}

		if (newValue == null || newValue.length() == 0 || newValue.matches(".*\\d.*")) {
			countryList.getCreateCountryButton().setDisable(true);
			countryList.getNameTextField().setId("inputNotOk");
		}
	}

	public void validateNameFieldState(String newValue) {
		if (newValue != null && !newValue.matches(".*\\d.*")) {
			stateList.getNameTextField().setId("inputOk");
			nameOK = true;
			if (nameOK && areaOK && populationOK && stateCountryOK) {
				stateList.getCreate().setDisable(false);
			}
		}

		if (newValue == null || newValue.length() == 0 || newValue.matches(".*\\d.*")) {
			stateList.getCreate().setDisable(true);
			stateList.getNameTextField().setId("inputNotOk");
			nameOK = false;
		}
	}

	public void validateAreaFieldState(String newValue) {
		if (newValue != null && newValue.matches(".*\\d.*")) {
			stateList.getAreaTextField().setId("inputOk");
			areaOK = true;
			if (nameOK && areaOK && populationOK && stateCountryOK) {
				stateList.getCreate().setDisable(false);
			}
		}

		if (newValue == null || newValue.length() == 0 || newValue.matches(".*[a-zA-Z]+.*")) {
			stateList.getCreate().setDisable(true);
			stateList.getAreaTextField().setId("inputNotOk");
			areaOK = false;
		}
	}

	public void validatePopulationFieldState(String newValue) {
		if (newValue != null && newValue.matches(".*\\d.*")) {
			stateList.getPopulationTextField().setId("inputOk");
			populationOK = true;
			if (nameOK && areaOK && populationOK && stateCountryOK) {
				stateList.getCreate().setDisable(false);
			}
		}

		if (newValue == null || newValue.length() == 0 || newValue.matches(".*[a-zA-Z]+.*")) {
			stateList.getCreate().setDisable(true);
			stateList.getPopulationTextField().setId("inputNotOk");
			populationOK = false;
		}
	}

	public void createState() {
		model.createState(stateList.getName(), stateList.getCountryChoiceBox().getSelectionModel().getSelectedItem(),
				stateList.getArea(), stateList.getGovernment(), stateList.getPopulation());
		stateTab.addListItem(model.getLastState());
		stateList.clearFields();

		model.getCountryByCountryID(
				stateList.getCountryChoiceBox().getSelectionModel().getSelectedItem().getCountryID())
				.addSate(model.getLastState());
	}

	public void updateState() {
		model.saveState(stateList.getStateID(), stateList.getName(),
				stateList.getCountryChoiceBox().getSelectionModel().getSelectedItem(), stateList.getArea(),
				stateList.getGovernment(), stateList.getPopulation());
		stateTab.updateListItem(model.getStateListIndexOfID(stateList.getStateID()),
				model.getStateByStateID(stateList.getStateID()));

	}

	public void deleteState() {

		stateTab.deleteListItem(model.deleteState(stateList.getStateID()));
		model.getCountryByCountryID(
				stateList.getCountryChoiceBox().getSelectionModel().getSelectedItem().getCountryID())
				.deleteState(model.getCountryListIndexOfID(
						stateList.getCountryChoiceBox().getSelectionModel().getSelectedItem().getCountryID()));

	}

	public void addState() {
		stateList.clearFields();
		stateList.setVisibilityCreate();
	}

	public void createCountry() {
		model.createCountry(countryList.getName());
		countryTab.addListItem(model.getLastCountry());
		countryList.clearFields();
	}

	public void saveCountry() {

		model.saveCountry(countryTab.getCountryID(), countryList.getName());
		countryTab.updateListItem(model.getCountryByCountryID(countryTab.getCountryID()));
		if (model.getStateListSize() != 0) {
			stateTab.updateListItem(model.getStateListIndexOfCountryID(countryTab.getCountryID()),
					model.getStateByCountryID(countryTab.getCountryID()));
			stateList.updateCountryChoiceBox(model.getCountryByCountryID(countryTab.getCountryID()));
		}
	}

	public void deleteCountry() {
		if (model.deleteStateByCountry(model.getCountryByCountryID(countryTab.getCountryID())).size() > 0) {
			for (int i = 0; i < model.deleteStateByCountry(model.getCountryByCountryID(countryTab.getCountryID()))
					.size(); i++) {
				stateTab.deleteListItem(model.deleteState(
						model.deleteStateByCountry(model.getCountryByCountryID(countryTab.getCountryID())).get(i)));
			}
		}
		countryTab.deleteListItem(model.deleteCountry(countryTab.getCountryID()));
	}

	public void addCountry() {
		countryList.clearFields();
		countryList.setVisibilityCreate();
	}
}
